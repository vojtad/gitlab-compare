# gitlab-compare

Compares merge request, commit, issue and pipeline count between gitlab.com and self-hosted GitLab instance. Prints projects with differences.

## Prepare

- Run `bundle install` to install required Ruby gems.
- Set following ENV variables in a shell. 

## Compare GitLab instances
- Run `ruby gitlab_compare.rb`.
  - All projects in `$TOP_LEVEL_GROUP` will be checked. All projects must exist in both gitlab.com and the self-managed instance.

```sh
export GITLAB_COM_ACCESS_TOKEN=
export SELF_MANAGED_HOST=
export SELF_MANAGED_ACCESS_TOKEN=
export TOP_LEVEL_GROUP=
```

### How to fix broken import?

- Export project manually from gitlab.com.
    - Go to project page on gitlab.com.
    - Open **Settings** -> **General**.
    - In **Advanced** click on **Export project**.
- Download project export when you receive and e-mail with export link.
- Go to self-hosted GitLab instance and import the project.
    - Go to **Projects** dashboard and click on **New Project**.
    - Click on **Import project**.
    - Click on **GitLab export**.
        - If it's not there go to **Admin Area**.
        - Open **Settings** -> **General**.
        - In **Import and export settings** enable **GitLab export** in **Import sources**.
    - Enter project details and select downloaded export from gitlab.com.
    - Click on **Import project**.
    - Wait.

## Migrate CI variables

All CI variables will be migrated except for archived projects.

- Run `ruby gitlab_variables.rb`.
