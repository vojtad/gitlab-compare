# frozen_string_literal: true

require 'httpx'
require 'cgi'

GITLAB_COM_ACCESS_TOKEN = ENV['GITLAB_COM_ACCESS_TOKEN']
SELF_MANAGED_HOST = ENV['SELF_MANAGED_HOST']
SELF_MANAGED_ACCESS_TOKEN = ENV['SELF_MANAGED_ACCESS_TOKEN']
TOP_LEVEL_GROUP = ENV['TOP_LEVEL_GROUP']

def gitlab_api_get(host: nil, path: nil, url: nil, params: {}, headers: {})
  host = case host
         when :self_managed
           SELF_MANAGED_HOST
         when :gitlab
           'gitlab.com'
         else
           host
         end

  url = "https://#{host}/api/v4/#{path}" if url.nil?

  private_token = case host
                  when 'gitlab.com'
                    GITLAB_COM_ACCESS_TOKEN
                  when SELF_MANAGED_HOST
                    SELF_MANAGED_ACCESS_TOKEN
                  end

  response = HTTPX.get(
    url,
    params: params,
    headers: { 'PRIVATE-TOKEN': private_token }.merge(headers)
  )

  [
    JSON.parse(response.body.to_s),
    {
      links: response.headers['link']&.split(',')&.to_h do |link|
        link_match = link.match(/<([^>]+)>; rel="([^"]+)"/)
        [link_match[2], link_match[1]]
      end || {},
      page: response.headers['x-page']&.to_i,
      total: response.headers['x-total']&.to_i,
      total_pages: response.headers['x-total-pages']&.to_i
    }
  ]
end

def get_variables_from_gitlab_com(resource_type:, resource_name:)
  encoded_resource_name = CGI.escape(resource_name)

  response = HTTPX.get(
    "https://gitlab.com/api/v4/#{resource_type}/#{encoded_resource_name}/variables",
    headers: { 'PRIVATE-TOKEN': GITLAB_COM_ACCESS_TOKEN }
  )

  if response.status != 200
    puts response.body.to_s

    return []
  end

  JSON.parse(response.body.to_s)
end

def set_variables_on_self_managed(resource_type:, resource_name:, variables:)
  encoded_resource_name = CGI.escape(resource_name)

  variables.each do |variable|
    puts "  setting variable #{variable['key']}..."

    response = HTTPX.post(
      "https://#{SELF_MANAGED_HOST}/api/v4/#{resource_type}/#{encoded_resource_name}/variables",
      form: variable,
      headers: { 'PRIVATE-TOKEN': SELF_MANAGED_ACCESS_TOKEN }
    )

    puts "    http status = #{response.status}"
  end
end

def list_group_resources_on_gitlab_com(parent_group:, resource_type:)
  all_resources = []

  url = "https://gitlab.com/api/v4/groups/#{CGI.escape(parent_group)}/#{resource_type}"

  loop do
    resources, pagination = gitlab_api_get(
      host: :gitlab,
      url: url,
      params: {
        all_available: true,
        pagination: 'keyset',
        per_page: 50,
        order_by: 'id'
      }
    )

    all_resources += resources
    url = pagination[:links]['next']

    break if url.nil?
  end

  all_resources
end

def migrate_variables(resource_type:, resource_name:)
  variables = get_variables_from_gitlab_com(resource_type: resource_type, resource_name: resource_name)
  set_variables_on_self_managed(resource_type: resource_type, resource_name: resource_name, variables: variables)
end

def migrate_project(project:)
  puts "Migrating project #{project}..."

  migrate_variables(resource_type: 'projects', resource_name: project)
end

def migrate_group(group:)
  puts "Migrating group #{group}..."

  migrate_variables(resource_type: 'groups', resource_name: group)

  list_group_resources_on_gitlab_com(parent_group: group, resource_type: 'projects').each do |child_project|
    migrate_project(project: child_project['path_with_namespace'])
  end

  list_group_resources_on_gitlab_com(parent_group: group, resource_type: 'subgroups').each do |child_group|
    migrate_group(group: child_group['full_path'])
  end
end

migrate_group(group: TOP_LEVEL_GROUP)
