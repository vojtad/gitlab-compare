# frozen_string_literal: true

require 'httpx'
require 'cgi'

GITLAB_COM_ACCESS_TOKEN = ENV['GITLAB_COM_ACCESS_TOKEN']
SELF_MANAGED_HOST = ENV['SELF_MANAGED_HOST']
SELF_MANAGED_ACCESS_TOKEN = ENV['SELF_MANAGED_ACCESS_TOKEN']
TOP_LEVEL_GROUP = ENV['TOP_LEVEL_GROUP']

def gitlab_api_get(host: nil, path: nil, url: nil, params: {}, headers: {})
  host = case host
         when :self_managed
           SELF_MANAGED_HOST
         when :gitlab
           'gitlab.com'
         else
           host
         end

  url = "https://#{host}/api/v4/#{path}" if url.nil?

  private_token = case host
                  when 'gitlab.com'
                    GITLAB_COM_ACCESS_TOKEN
                  when SELF_MANAGED_HOST
                    SELF_MANAGED_ACCESS_TOKEN
                  end

  response = HTTPX.get(
    url,
    params: params,
    headers: { 'PRIVATE-TOKEN': private_token }.merge(headers)
  )

  [
    JSON.parse(response.body.to_s),
    {
      links: response.headers['link']&.split(',')&.to_h do |link|
        link_match = link.match(/<([^>]+)>; rel="([^"]+)"/)
        [link_match[2], link_match[1]]
      end || {},
      page: response.headers['x-page']&.to_i,
      total: response.headers['x-total']&.to_i,
      total_pages: response.headers['x-total-pages']&.to_i
    }
  ]
end

all_gitlab_com_projects = []
url = "https://gitlab.com/api/v4/groups/#{TOP_LEVEL_GROUP}/projects"

loop do
  puts url
  projects, pagination = gitlab_api_get(
    host: :gitlab,
    url: url,
    params: {
      include_subgroups: true,
      pagination: 'keyset',
      per_page: 50,
      order_by: 'id'
    }
  )

  all_gitlab_com_projects += projects
  url = pagination[:links]['next']

  break if url.nil?
end

errors = {}

puts "Loaded #{all_gitlab_com_projects.size} projects from gitlab.com."

all_gitlab_com_projects.each do |project|
  project_path = project['path_with_namespace']
  url_encoded_project_path = CGI.escape(project_path)

  puts "Checking #{project_path}..."

  %w[merge_requests pipelines issues commits].each do |resource|
    path = "projects/#{url_encoded_project_path}/#{resource}"

    _, gitlab_com = gitlab_api_get(host: :gitlab, path: path)
    _, self_managed = gitlab_api_get(host: :self_managed, path: path)

    if gitlab_com[:total] != self_managed[:total]
      message = "  ! #{resource} mismatch for #{project_path}: gitlab.com = #{gitlab_com[:total]}, #{SELF_MANAGED_HOST} = #{self_managed[:total]}"

      errors[project_path] ||= []
      errors[project_path] << message

      puts message
    end
  end
end

puts '=== errors'

errors.each do |project, project_errors|
  puts project
  puts "  https://gitlab.com/#{project}/edit#js-project-advanced-settings"
  puts project_errors
end
